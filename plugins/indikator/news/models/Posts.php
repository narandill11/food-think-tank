<?php namespace Indikator\News\Models;

use Model;
use File;
use App;
use Html;
use Flash;
use DB;
use Mail;
use Lang;
use RainLab\Translate\Models\Locale;
use BackendAuth;
use October\Rain\Exception\ApplicationException;
use Ms1Design\Root\Models\Settings;

class Posts extends Model
{
    use \October\Rain\Database\Traits\Sluggable;
    use \October\Rain\Database\Traits\Validation;

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    protected $table = 'news_posts';

    public $rules = [
        'title'   => 'required|between:1,100',
        'content' => 'required',
        'status'  => 'required|between:1,3|numeric'
    ];

    protected $slugs = ['slug' => 'title'];

    public $translatable = ['title', 'introductory', 'content'];

    protected $dates = ['published_at'];

    public function beforeCreate()
    {
        $this->statistics = 0;

        if ($this->published_at == '') {
            $this->published_at = date('Y-m-d H:i:00');
        }
    }

    public function beforeUpdate()
    {
        unset($this->statistics);
    }

    public function filterFields($fields, $context = null)
    {
        // turn off sending e-mail to subscribers if post is arleady published
        if ($this->status == 1) {
            $fields->send->value = false;
            $fields->send->comment = Lang::get('ms1design.root::lang.news.send_disabled');
            $fields->social->value = false;
            $fields->social->comment = Lang::get('ms1design.root::lang.news.social_disabled');
        }
        if ($this->published_social){
            $fields->social->value = false;
            $fields->social->disabled = true;
            $fields->social->comment = Lang::get('ms1design.root::lang.news.social_published');
        }
    }

    public function beforeSave()
    {
        if ($this->send && $this->send != '') {
            //$locale = App::getLocale();
            $users = DB::table('news_subscribers')->get();

            foreach ($users as $user) {
                $locale = Locale::where('id', $user->locale_id)->first()->code;
                if (empty($locale)){
                    $locale = "pl_PL";
                }
                if (!File::exists('plugins/indikator/news/views/mail/email_'.$locale.'.htm')) {
                    $locale = 'en_US';
                }
                $params = [
                    'name'          => $user->name,
                    'email'         => $user->email,
                    'title'         => $this->getTranslateAttribute('title', $locale),
                    'slug'          => $this->slug,
                    'introductory'  => $this->getTranslateAttribute('introductory', $locale),
                    'content'       => $this->getTranslateAttribute('content', $locale),
                    'image'         => $this->image,
                    'url'           => 'www.foodthinktank.pl/post/',
                ];

                $this->email = $user->email;
                $this->name = $user->name;
                $this->title = $this->getTranslateAttribute('title', $locale);

                Mail::send('indikator.news::mail.email_'.$locale, $params, function($message) {
                    $message->from('fft@foodthinktank.pl', 'Food Think Tank');
                    $message->sender('fft@foodthinktank.pl', 'Food Think Tank');
                    $message->replyTo('fft@foodthinktank.pl', 'Food Think Tank');
                    $message->to($this->email, $this->name)->subject($this->title);
                });

                DB::table('news_subscribers')->where('id', $user->id)->update(array('statistics' => ($user->statistics + 1)));
            }

            unset($this->email, $this->name);
        }

        if ($this->send) {
            $this->send = 1;
        } else {
            $this->send = 2;
        }

        if ($this->created_by == '') {
            $user = BackendAuth::getUser();
            $this->created_by = $user->first_name . " " . $user->last_name;
        }

        if ($this->social && !$this->published_social){
            $this->publishSocial();
        }
    }

    // =========================================================================================================
    // Publish post to social networks
    // =========================================================================================================

    public function publishSocial()
    {   
        // SETTINGS GLOBAL
        $home_url   = 'http://'.$_SERVER['HTTP_HOST'].'/';
        $settings   = Settings::instance();

        // FACEBOOK API SETTINGS
        $fb = [
            'app' => [
                'id' => $settings->fb_app_id, // '230376033981527',
                'secret' => $settings->fb_app_secret, // '4666db1d4a3a52eefea091ec8c0e5634',
            ],
            'page' => [
                'id' => $settings->fb_page_id, //'1585152581803730',
                'token' => $settings->fb_page_token, //'CAADRhpmrlFcBACPPUSMk4FevZBwMJSJw0zRpxNxCwnFB37Muq0E1XSrqQVAEA445fjGKL6VRrIgM4MxPk3KQfM4RoyIkKgFZCoZCKZALgZAlAQZBH1Yh7Jc7Lnr38NAEExaO60CrxISLVZAuPsF7h8TBDJiYxAZB9jkLa7xLMOLMTSTTyv3WZCDpdwNZCECiRqtXEZD',
            ],
            'profile' => [
                'id' => $settings->fb_profile_id, //'100000604801117',
                'link' => $settings->fb_profile_link, //'https://www.facebook.com/narandill'
            ]
        ];

        // When its not working:
        // Acces Token is working for 2 months only, so
        // we need to refresh Acces Token () for Facebook App caling this link:
        
        $token_check = $this->CheckToken('https://graph.facebook.com/oauth/access_token?client_id='.$fb['app']['id'].'&client_secret='.$fb['app']['secret'].'&grant_type=fb_exchange_token&fb_exchange_token='.$fb['page']['token']);
        
        // News settings
        $post_prefix = $settings->post_prefix;

        // Start Facebook Publish
        $br = chr(10);
        
        $data = [
            'picture' => $home_url."storage/app/media".$this->image,
            'link' => $home_url.$post_prefix.$this->slug,
            'message' => Html::strip($this->introductory).$br.$br.'Zobacz: '.$home_url.$post_prefix.$this->slug.$br.$br,
            'description' => (strlen(Html::strip($this->content)) > 50 ) ? Html::strip($this->content) : Html::strip($this->introductory),
            'name' => $this->title,
            'author' => $this->created_by,
            'type' => 'link',
            'access_token' => $fb['page']['token'],
            'from' => $fb['page']['id'],
            'object_id' => $this->id,
            'status_type' => 'wall_post',
            // 'admin_creator' => [
            //     'id' => 11,
            //     'name' => $this->created_by,
            // ],
        ];

        // Facebook Post URL
        $post_url = 'https://graph.facebook.com/'.$fb['page']['id'].'/feed';

        // Send post to facebook page
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $post_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $return = curl_exec($ch);
        curl_close($ch);
        // END FACEBOOK

        if ( isset(json_decode($return)->id) ){
            Flash::success('Wpis został opublikowany na Twoim facebooku :-)');
            $this->published_social = true;
        } else {
            Flash::error('O nie! Wystąpił błąd! Szybko biegnij po Mieszka ;-)');
            throw new ApplicationException("O nie! Wystąpił błąd!\nSzybko biegnij po Mieszka ;-)\n\n".json_encode($return));
        }
    }

    public function CheckToken($url){
        $token = $this->CurlURL($url);
        if ( isset($token->error) ){
            if ( isset($token->error->code) && $token->error->code == 190){
                $refresh = $this->CurlURL($url);
                throw new ApplicationException("Facebook token expired! (code ".$token->error->code.")\n\nRefresh:" . json_encode($refresh) . "\n\nVisit: " . $url);
            } else {
                throw new ApplicationException("Facebook token error! \n\n".json_encode($token->error) . "Visit: " . $url);
            }
        } else {
            return true;
        }
    }

    public function CurlURL($url){
        $curlSession = curl_init();
        curl_setopt($curlSession, CURLOPT_URL, $url);
        curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
        $jsonData = json_decode(curl_exec($curlSession));
        curl_close($curlSession);
        return $jsonData;
    }
}
