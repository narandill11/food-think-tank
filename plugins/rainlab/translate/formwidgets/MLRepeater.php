<?php namespace RainLab\Translate\FormWidgets;

use Backend\Classes\FormField;
use Backend\Classes\FormWidgetBase;
use RainLab\Translate\Models\Locale;

/**
 * Repeater Form Widget
 */
class MLRepeater extends FormWidgetBase
{
    const INDEX_PREFIX = '___index_';

    //
    // Configurable properties
    //

    /**
     * @var array Form field configuration
     */
    public $form;

    /**
     * @var string Prompt text for adding new items.
     */
    public $prompt = 'Add new item';

    /**
     * @var bool Items can be sorted.
     */
    public $sortable = false;

    //
    // Object properties
    //

    /**
     * {@inheritDoc}
     */
    protected $defaultAlias = 'mlrepeater';

    /**
     * @var int Count of repeated items.
     */
    protected $indexCount = 0;

    /**
     * @var array Collection of form widgets.
     */
    protected $formWidgets = [];

     /**
      * @var bool Stops nested repeaters populating from previous sibling.
      */
    protected static $onAddItemCalled = false;

    public $originalAssetPath;
    public $originalViewPath;

    /**
     * {@inheritDoc}
     */
    public function init()
    {
        $this->initLocale();
        $this->fillFromConfig([
            'form',
            'prompt',
            'sortable',
        ]);

        if (!self::$onAddItemCalled) {
            $this->processExistingItems();
        }
    }

    /**
     * {@inheritDoc}
     */
    public function render()
    {
        $this->actAsParent();
        $parentContent = parent::render();
        $this->actAsParent(false);

        if (!$this->isAvailable) {
            return $parentContent;
        }
        $this->prepareLocaleVars();
        
        $this->vars['repeater'] = $parentContent;

        return $this->makePartial('mlrepeater');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        // $this->vars['indexName'] = self::INDEX_PREFIX.$this->formField->getName(false).'[]';
        // $this->vars['prompt'] = $this->prompt;
        // $this->vars['formWidgets'] = $this->formWidgets;
        parent::prepareVars();
        $this->prepareLocaleVars();
    }

    /**
     * {@inheritDoc}
     */
    protected function loadAssets()
    {
        $this->addCss('css/repeater.css', 'core');
        $this->addJs('js/repeater.js', 'core');

        $this->actAsParent();
        parent::loadAssets();
        $this->actAsParent(false);

        if (Locale::isAvailable()) {
            $this->loadLocaleAssets();
            $this->addJs('js/mlswitcher.js');
        }
    }

    protected function actAsParent($switch = true)
    {
        if ($switch) {
            $this->originalAssetPath = $this->assetPath;
            $this->originalViewPath = $this->viewPath;
            $this->assetPath = '/modules/backend/formwidgets/repeater/assets';
            $this->viewPath = base_path().'/modules/backend/formwidgets/repeater/partials';
        }
        else {
            $this->assetPath = $this->originalAssetPath;
            $this->viewPath = $this->originalViewPath;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getSaveValue($value)
    {
        return (array) $value;
    }

    protected function processExistingItems()
    {
        $loadValue = $this->getLoadValue();
        if (is_array($loadValue)) {
            $loadValue = array_keys($loadValue);
        }

        $itemIndexes = post(self::INDEX_PREFIX.$this->formField->getName(false), $loadValue);

        if (!is_array($itemIndexes)) return;

        foreach ($itemIndexes as $itemIndex) {
            $this->makeItemFormWidget($itemIndex);
            $this->indexCount = max((int) $itemIndex, $this->indexCount);
        }
    }

    protected function makeItemFormWidget($index = 0)
    {
        $loadValue = $this->getLoadValue();
        if (!is_array($loadValue)) $loadValue = [];

        $config = $this->makeConfig($this->form);
        $config->model = $this->model;
        $config->data = array_get($loadValue, $index, []);
        $config->alias = $this->alias . 'Form'.$index;
        $config->arrayName = $this->formField->getName().'['.$index.']';

        $widget = $this->makeWidget('Backend\Widgets\Form', $config);
        $widget->bindToController();

        return $this->formWidgets[$index] = $widget;
    }

    public function onAddItem()
    {
        self::$onAddItemCalled = true;

        $this->indexCount++;

        $this->prepareVars();
        $this->vars['widget'] = $this->makeItemFormWidget($this->indexCount);
        $this->vars['indexValue'] = $this->indexCount;

        $itemContainer = '@#'.$this->getId('items');
        return [$itemContainer => $this->makePartial('repeater_item')];
    }

    public function onRemoveItem()
    {
        // Useful for deleting relations
    }

}
